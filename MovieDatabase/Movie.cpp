#include <fstream>
#include <vector>
#include <sstream>
#include <algorithm>
#include <regex>
#include "Movie.h"

Movie::Movie(std::string t, int y, std::string c, std::string g, int d, double r) {
    title = t;
    year = y;
    certificate = c;
    genre = g;
    duration = d;
    rating = r;
}

Movie::Movie(std::string line) {
    std::string t;
    std::string c;
    std::string g;
    std::vector<std::string> result;
    std::regex re(",(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)");

    std::sregex_token_iterator it(line.begin(), line.end(), re, -1);
    std::sregex_token_iterator reg_end;

    for (; it != reg_end; ++it) {
        result.push_back(it->str());
    }

    title = result.at(0);
    year = std::stoi(result.at(1));
    certificate = result.at(2);
    genre = result.at(3);
    duration = std::stoi(result.at(4));
    rating = std::stod(result.at(5));
}

std::string Movie::getTitle() {
    return title;
}

int Movie::getYear() {
    return year;
}

std::string Movie::getCertificate() {
    return certificate;
}

std::string Movie::getGenre() {
    return genre;
}

int Movie::getDuration() {
    return duration;
}

double Movie::getRating() {
    return rating;
}

void Movie::print(){
    std::cout << title << " ," << year << " ," << certificate << " ," << genre << " ,"
         << duration << " ," << rating << std::endl;
}

bool Movie::compareYear(Movie m1, Movie m2) {
    return (m1.year<m2.year);
}

bool Movie::compareDuration(Movie m1, Movie m2) {
    return (m1.duration>m2.duration);
}

//https://www.tutorialspoint.com/parsing-a-comma-delimited-std-string-in-cplusplus
//https://www.geeksforgeeks.org/converting-strings-numbers-cc/
/*
Since C++11 converting string to floating-point values (like double) is available with functions:
stof - convert str to a float
        stod - convert str to a double
        stold - convert str to a long double

        As conversion of string to int was also mentioned in the question, there are the following functions in C++11:
stoi - convert str to an int
        stol - convert str to a long
        stoul - convert str to an unsigned long
        stoll - convert str to a long long
        stoull - convert str to an unsigned long long*/
//https://stackoverflow.com/questions/3776458/split-a-comma-separated-string-with-both-quoted-and-unquoted-strings
//https://stackabuse.com/regex-splitting-by-character-unless-in-quotes/
//https://www.informit.com/articles/article.aspx?p=2064649&seqNum=7